import psycopg2
import json


def sql_connection():
    with open('./bd/postgres_credentials.json', 'r') as f:
        postgres_credentials = json.load(f)
        host = postgres_credentials['host']
        database = postgres_credentials['database']
        user = postgres_credentials['user']
        password = postgres_credentials['password']
        port = postgres_credentials['port']
    try:
        conn = psycopg2.connect(host=host, database=database, port=port,
                                user=user, password=password)
        return conn
    except Exception:
        print(Exception)

def commit(conn, sql_statement):
    cur = conn.cursor()
    file = open('data/strava_activities_bd.log',"a")
    try:
        cur.execute(sql_statement)
    except:
        print("Error :", sql_statement)
        file.write("Error :" +sql_statement)
        file.write("\n")

    conn.commit()
    cur.close()
    file.close()
    return print("statement commmited")

def fetch(conn, sql_statement):
    cur = conn.cursor()
    cur.execute(sql_statement)
    output = cur.fetchall()
    cur.close()
    return output

def insert_statement(table_name, record):
    columns = ','.join(list(record.keys()))
    values  = str(tuple(record.values()))
    statement = """INSERT INTO {} ({}) VALUES {};""".format(table_name, columns, values)
    #print(statement)
    return statement