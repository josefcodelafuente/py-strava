import requests
import pandas as pd

URL = "https://www.strava.com/api/v3"


# funcion que recupera las actividades desde una fecha dada
def request_activities(access_token, start_date = False):
    end_point = "athlete/activities"
    activities_url = URL+"/"+end_point
    page = 1
    activities = pd.DataFrame(
    columns = [
            "id",
            "name",
            "start_date_local",
            "type",
            "distance",
            "moving_time",
            "elapsed_time",
            "total_elevation_gain",
            "end_latlng",
            "kudos_count",
            "external_id"
        ]
    )
    while True:
    
        # Loop through all activities
        header = {"Authorization": "Bearer {}".format(access_token)}
        param = {'per_page': 200, 'page': page }
        if start_date:
            param['after'] = start_date
        try:
            r = requests.get(activities_url, headers=header, params=param).json()
            print("LLamando al API Strava ", end_point)
            if (not r):
                break
    
            # otherwise add new data to dataframe
            for x in range(len(r)):
                activities.loc[x + (page-1)*200,'id'] = r[x]['id']
                activities.loc[x + (page-1)*200,'name'] = r[x]['name']
                activities.loc[x + (page-1)*200,'start_date_local'] = r[x]['start_date_local']
                activities.loc[x + (page-1)*200,'type'] = r[x]['type']
                activities.loc[x + (page-1)*200,'distance'] = r[x]['distance']
                activities.loc[x + (page-1)*200,'moving_time'] = r[x]['moving_time']
                activities.loc[x + (page-1)*200,'elapsed_time'] = r[x]['elapsed_time']
                activities.loc[x + (page-1)*200,'total_elevation_gain'] = r[x]['total_elevation_gain']
                activities.loc[x + (page-1)*200,'end_latlng'] = r[x]['end_latlng']
                activities.loc[x + (page-1)*200,'kudos_count'] = r[x]['kudos_count']
                activities.loc[x + (page-1)*200,'external_id'] = r[x]['external_id']
            # increment page
            page += 1
        except:
            print(r)
            raise Exception("Error en el endpoint: " +end_point)
        
    return activities


#funcion que recupera los kudos de una actividad dada
def request_kudos(access_token, activity_id):
    end_point = "activities/{}/kudos".format(activity_id)
    url_kudos = URL+"/"+end_point
    page = 1
    kudos = pd.DataFrame(
    columns = [
            "firstname",
            "lastname"
        ]
    )
    while True:
        header = {"Authorization": "Bearer {}".format(access_token)}
        params = {'per_page': 30, 'page': page}
        try:
            r = requests.get(url_kudos, headers=header, params=params).json()
            print("LLamando al API Strava ", end_point)
            if (not r):
                break
            # otherwise add new data to dataframe
            for x in range(len(r)):
                #kudos.loc[x + (page-1)*30,'resource_state'] = r[x]['resource_state']
                kudos.loc[x + (page-1)*30,'firstname'] = r[x]['firstname']
                kudos.loc[x + (page-1)*30,'lastname'] = r[x]['lastname']
            # increment page
            page += 1
        except:
            raise Exception("Error en el endpoint: " +end_point)
    return kudos
