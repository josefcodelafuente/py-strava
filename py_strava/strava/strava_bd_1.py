import sqlite3
from sqlite3 import Error
from sqlite3.dbapi2 import Statement


def sql_connection(bbdd):
    try:
        conn = sqlite3.connect(bbdd)
        return conn
    except Error:
        print(Error)

def commit(conn, sql_statement):
    cur = conn.cursor()
    file = open('data/strava_activities_bd.log',"a")
    try:
        cur.execute(sql_statement)
    except:
        print("Error :", sql_statement)
        file.write("Error :" +sql_statement)
        file.write("\n")

    conn.commit()
    cur.close()
    file.close()
    return print("statement commmited")

def fetch(conn, sql_statement):
    cur = conn.cursor()
    cur.execute(sql_statement)
    output = cur.fetchall()
    cur.close()
    return output

def insert_statement(table_name, record):
    columns = ','.join(list(record.keys()))
    values  = str(tuple(record.values()))
    statement = """INSERT INTO {} ({}) VALUES {};""".format(table_name, columns, values)
    #print(statement)
    return statement



