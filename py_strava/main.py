from datetime import datetime
import strava.strava_token_1 as stravaToken
import strava.strava_bd_postgres as stravaBBDD
import strava.strava_activities as stravaActivities
import strava.strava_fechas as stravaFechas
import csv

# Creamos la conexión a la BD
# STRAVA_BD = 'bd/strava.sqlite' -- variable solo para sqlite
STRAVA_ACTIVITIES_LOG = './data/strava_activities.log'
STRAVA_TOKEN_JSON = './json/strava_tokens.json'

create_table_activities = """
    CREATE TABLE IF NOT EXISTS Activities (
    id_activity INTEGER PRIMARY KEY,
    name TEXT,
    start_date_local TEXT,
    type TEXT,
    distance REAL,
    moving_time REAL,
    elapsed_time REAL,
    total_elevation_gain REAL,
    end_latlng TEXT,
    kudos_count INTEGER,
    external_id INTEGER
    )"""

create_table_kudos = """
    CREATE TABLE IF NOT EXISTS Kudos (
    id_kudos INTEGER PRIMARY KEY AUTOINCREMENT,
    resource_state TEXT,
    firstname TEXT,
    lastname TEXT,
    id_activity INTEGER,
    FOREIGN KEY (id_activity) REFERENCES Activities(id_activity)
    )"""

# Creamos y nos conectamos a la bbdd donde almacenar las Actividades 
# y los Kudos
# conn = stravaBBDD.sql_connection(STRAVA_BD)
"""
conn = stravaBBDD.sql_connection()
stravaBBDD.commit(conn,'DROP TABLE IF EXISTS Activities')
stravaBBDD.commit(conn,'DROP TABLE IF EXISTS Kudos')
stravaBBDD.commit(conn, create_table_activities)
stravaBBDD.commit(conn, create_table_kudos)
"""

# Nos conectamoa a la BBDD donde almacenar las Actividades y los Kudos
# conn = stravaBBDD.sql_connection(STRAVA_BD) -- sqlite
conn = stravaBBDD.sql_connection()

#print("refresh token")
strava_tokens = stravaToken.refreshToken(stravaToken.getTokenFromFile(STRAVA_TOKEN_JSON), 
                                         STRAVA_TOKEN_JSON)
access_token = strava_tokens['access_token']
print("Access Token = {}\n".format(access_token))

# obtener la fecha de la ultima actividad cargada
lt = stravaFechas.last_timestamp(STRAVA_ACTIVITIES_LOG)
seconds = stravaFechas.timestamp_to_unix(lt)

try:
    # obtener las actividades
    activities = stravaActivities.request_activities(access_token, seconds)

    # Recuperamos las Actividades y las cargamos en la bbdd
    for k in range(len(activities)):
        record_a = dict()
        record_a['id_activity'] = activities.loc[k,'id']
        record_a['name'] = activities.loc[k,'name']
        record_a['start_date_local'] = activities.loc[k,'start_date_local']
        record_a['type'] = activities.loc[k,'type']
        record_a['distance'] = activities.loc[k,'distance']
        record_a['moving_time'] = activities.loc[k,'moving_time']
        record_a['elapsed_time'] = activities.loc[k,'elapsed_time']
        record_a['total_elevation_gain'] = activities.loc[k,'total_elevation_gain']
        record_a['end_latlng'] = str(activities.loc[k,'end_latlng'])
        record_a['kudos_count'] = activities.loc[k,'kudos_count']
        record_a['external_id'] = activities.loc[k,'external_id']
        stravaBBDD.commit(conn, stravaBBDD.insert_statement("Activities", record_a))

except Exception as ex:
    print(ex)
    exit(0)


# recuperamos los kudos de cada actividad y los cargamos en la bbdd
activities_lst = activities['id'].tolist()
for activity in activities_lst:
    # Obtener los kudos dados a una actividad
    try:
        kudos = ()
        #print("Buscando ", activity)
        kudos = stravaActivities.request_kudos(access_token, activity)
        for k in range(len(kudos)):
            record = dict()
            record['id_activity'] = activity
            #record['resource_state'] = kudos.loc[k,'resource_state']
            record['firstname'] = kudos.loc[k,'firstname']
            record['lastname'] = kudos.loc[k,'lastname']
            stravaBBDD.commit(conn, stravaBBDD.insert_statement("Kudos", record))
    except Exception as ex:
        print(ex)
        exit(0)


# almacenar fecha actual
date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
with open(STRAVA_ACTIVITIES_LOG, 'a', newline = '\n') as a:
    csv_write = csv.writer(a)
    csv_write.writerow([date, len(activities_lst)])
a.close()
