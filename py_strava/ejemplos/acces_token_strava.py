import requests
import json

# Make Strava auth API call with your 
# client_code, client_secret and code
response = requests.post(
                    url = 'https://www.strava.com/oauth/token',
                    data = {
                            'client_id': 56852,
                            'client_secret': '6b229c286a12180a2acad07d23a6f43ae999d046',
                            'code': '896509433675a143c7a61b819dd8f5294888a4d9',
                            'grant_type': 'authorization_code'
                            }
                )

#Save json response as a variable
strava_tokens = response.json()
print(strava_tokens)

# Save tokens to file
with open('json/strava_tokens.json', 'w') as outfile:
    json.dump(strava_tokens, outfile)

# Open JSON file and print the file contents 
# to check it's worked properly
with open('json/strava_tokens.json') as check:
  data = json.load(check)

print(data)

