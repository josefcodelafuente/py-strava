import requests
from pandas.io.json import json_normalize
import json
import csv

# Get the tokens from file to connect to Strava
with open('json/strava_tokens.json') as json_file:
    strava_tokens = json.load(json_file)

# Loop through all activities
activites_url = "https://www.strava.com/api/v3/athlete/activities"
access_token = strava_tokens['access_token']
print("Access Token = {}\n".format(access_token))

header = {'Authorization': 'Bearer ' + access_token}
param = {'per_page': 200, 'page': 1}
my_dataset = requests.get(activites_url, headers=header, params=param).json()
#print(my_dataset)

df = json_normalize(my_dataset)
df.to_csv('data/strava_activities_all_fields.csv')