import strava.strava_bd_1 as stravaBBDD
import csv

# Creamos la conexión a la BD
STRAVA_BD = 'bd/strava.sqlite'
STRAVA_DATA_CSV = 'data/strava_data2.csv'

# Creamos las bbdd donde almacenar las Actividades y los Kudos
conn = stravaBBDD.sql_connection(STRAVA_BD)



#record = stravaBBDD.fetch(conn,'SELECT id_activity FROM Activities WHERE kudos_count > 0')
record = stravaBBDD.fetch(conn,'SELECT firstname, lastname, type, kudos.id_activity, start_date_local \
                                FROM Kudos, Activities \
                                WHERE kudos.id_activity=Activities.id_activity;')


fieldnames = ['FIRST_NAME', 'LAST_NAME', 'TIPO', 'ACTIVIDAD', 'START_DATE']

with open(STRAVA_DATA_CSV, mode='w') as file:
    employee_writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    employee_writer.writerow(fieldnames)
    for r in record:
        employee_writer.writerow(r)

conn.close()
file.close()
